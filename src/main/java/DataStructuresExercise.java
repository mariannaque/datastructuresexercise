
import java.util.*;
import java.util.stream.Collectors;

public class DataStructuresExercise {

    /**
     * A list of Number objects(index,value) is created to keep the index
     * of each number in numbers array. Then, the numberList is being
     * sorted and becomes sortedList.
     */
    public static void findAPair(int[] numbers, int sum){

        List<Number> numberList = new ArrayList<>();

        for (int i = 0; i < numbers.length; i++){
            Number number = new Number();
            number.index = i;
            number.value = numbers[i];
            numberList.add(number);
        }
        List<Number> sortedList;
        sortedList = numberList.stream().sorted(Comparator.comparing(number -> number.value)).collect(Collectors.toList());

       /*Every element(i=0) in sortedList is being a pair with each one of
         the next elements(j=i+1). The algorithm continues only if the
         elements' values are smaller than the sum.*/

        for (int i = 0; i < sortedList.size(); i++){
            for (int j = i + 1; j < sortedList.size(); j++){
                if (sortedList.get(i).value < sum && sortedList.get(j).value < sum){
                    int newSum = sortedList.get(i).value + sortedList.get(j).value;
                    if (newSum == sum)
                    {
                        System.out.println("Pair found at index " + sortedList.get(j).index + " and " + sortedList.get(i).index + "(" + sortedList.get(j).value + "+" + + sortedList.get(i).value + ")");
                    }
                }else{
                    break;
                }
            }
        }
    }

    /**
     * @param numbers is an array of integers.
     * @param target is the element we search for.
     * The first step is to check the value of the element in the middle of the
     * array. If target is greater than this element, we do the same process
     * in the right subarray and if not, we do the process in the left
     * subarray.
     */
    public static void binarySearch(int[] numbers, int target){
        int start = 0;
        int end = numbers.length - 1;

        while (end > start){
            int index = (end - start) / 2;
            if (numbers[index] == target){
                System.out.println("Target found at position " + index);
                return;
            }else if (numbers[index] > target){
                end = index - 1;
            }else{
                start = index + 1;
            }
        }
        System.out.println("Target not found");
    }

    /**
     * @param numbers is an array of integers.
     * @param target is the element we search for.
     * for each element of the numbers array we check if its value is equal
     * to target. If it is, we add its value to the results list. (We assume
     * that there are more than one elements equal to target)
     */
    public static void linearSearch(int[] numbers, int target){
        List<Integer> results = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++){
            if (numbers[i] == target){
                results.add(i);
            }
            // For unsorted input array comment out the below code
            else if (numbers[i] > target){
                break;
            }
        }
        if (results.size() == 0){
            System.out.println("Target not found.");
            return;
        }
        System.out.print("Target: ");
        for (int i = 0; i < results.size(); i++){

            System.out.print(results.get(i));
            //puts "," between the target indexes apart from the last one
            System.out.print(i < results.size() - 1 ? ", " : "");
        }
        System.out.println();
    }

    /**
     * @param numbers is an array of integers.
     * @param target is the element we search for.
     * Counts the elements with value equal to target.
     */
    public static void countOccurrences(int[] numbers, int target){
        //numbers is sorted
        int counter = 0;
        for (int i = 0; i < numbers.length; i++){
            if (numbers[i] == target){
                counter++;
            }else if (numbers[i] > target){
                break;
            }
        }
            System.out.println("Element " + target + " occurs " + counter +
                    " times.");
    }
}
