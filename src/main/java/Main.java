
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] numbers = {10, 9, 8, 11, 10, 15, 1, 10, 22};

        DataStructuresExercise.findAPair(numbers, 10);

        int[] sortedArray = Arrays.stream(numbers).sorted().toArray();

        DataStructuresExercise.linearSearch(sortedArray, 10);

        DataStructuresExercise.binarySearch(sortedArray, 10);

        DataStructuresExercise.countOccurrences(sortedArray, 10);
    }
}
